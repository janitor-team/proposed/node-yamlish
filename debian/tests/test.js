const test = require('tape')
const yamlish = require("../..");

const yaml = "some:\n  object:\n    - full\n    - of\n  pretty: things";

test('encode', t => {
    const res = yamlish.decode(yaml);
    t.deepEqual(res,{object:["full", "of"], pretty:"things"});
    t.end();
});
